import AbstractMigrationService from './abstractMigrationService'

export default class WunderlistMigrationService extends AbstractMigrationService {
	constructor() {
		super('wunderlist')
	}
}
