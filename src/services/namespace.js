import AbstractService from './abstractService'
import NamespaceModel from '../models/namespace'
import moment from 'moment'

export default class NamespaceService extends AbstractService {
	constructor() {
		super({
			create: '/namespaces',
			get: '/namespaces/{id}',
			getAll: '/namespaces',
			update: '/namespaces/{id}',
			delete: '/namespaces/{id}',
		});
	}

	processModel(model) {
		model.created = moment(model.created).toISOString()
		model.updated = moment(model.updated).toISOString()
		return model
	}

	modelFactory(data) {
		return new NamespaceModel(data)
	}
}